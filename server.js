const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const {persona} = require('./sequelize');

const app = express();

app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  try {
    return res.status(200).json({ok: true, message: 'Holis'});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
});

app.get('/personas', async (req, res) => {
  try {
    let Persona = await persona.findAll()
    return res.status(200).json({ok: true, Persona: Persona});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
});

app.get('/personas/:id', async (req, res) => {
  try {
    const {id} = req.params;
    let Persona = await persona.findByPk(id);
    return res.status(200).json({ok: true, Persona});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
})

app.post('/persona', async (req, res) => {
  try {
    const {nombres, apellidos, phone,} = req.body;
    let Persona = await persona.create({
      nombres,
      apellidos,
      phone
    })
    return res.status(200).json({ok: true, message: 'persona creada', persona});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
})

app.put('/personas/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const body = req.body;
    let Persona = await persona.findByPk(id)
    let update = await Persona.update(body)
    return res.status(200).json({ok: true, message: 'Datos actualizados', update});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
})

app.delete('/personas/:id', async (req, res) => {
  try {
    const {id} = req.params;
    let Persona = await persona.destroy({where: {id}});
    return res.status(200).json({ok: true, message: 'Persona eliminada', Persona});
  } catch (err) {
    return res.status(500).json({ok: false, message: err.message});
  }
})

let port = process.env.PORT || 3000;
app.listen(port, () => console.log(`server on port ${port}`));
