const Sql = require('sequelize');
const Persona = require('./models/Persona');
const dbUrl = 'mysql://root:' + '' + '@localhost:3306/api-agenda';
// const dbUrl = {
//   host: 'localhost',
//   user: 'root',
//   password: '',
//   port: 3307
// }
const sequelize = new Sql(dbUrl);
const persona = Persona(sequelize, Sql);
sequelize.sync()
  .then(() => {
    console.log('tabla creada')
  })

module.exports = {
  persona
}
